#![allow(dead_code)]

use std::collections::VecDeque;
use std::fs::*;
use std::io::*;
use std::result::Result;

pub enum SplitterState{
    Start, 
    Comment,
    Tag(String),
    Source(String),
}

pub struct Splitter{
    state:SplitterState,
    pub tokens:VecDeque<SplitterState>,
}

impl Splitter{
    pub fn new()->Splitter{
        Splitter{
            state:SplitterState::Start,
            tokens:VecDeque::new(),
        }
    }
    pub fn parse_source(mut self, source:&String) -> Splitter{
        let mut buffer = String::new();
        let mut iterator = source.chars().enumerate().peekable(); 
        let mut has_next = true; 

        while has_next{
            has_next = match iterator.next(){
                Some((_i,c))=>{
                    self.state = match self.state{
                        SplitterState::Start =>{
                            match iterator.peek(){
                                Some((_,nc)) =>{
                                    if c == '/' && *nc == '/'{
                                        SplitterState::Comment
                                    }else{
                                        SplitterState::Start
                                    }
                                },
                                None => {
                                    SplitterState::Start
                                },
                            }
                        },
                        SplitterState::Comment =>{
                            match iterator.peek(){
                                Some((_,nc))=>{
                                    if c=='/' && *nc =='[' {
                                        SplitterState::Tag(String::new())
                                    }else if  c=='/' && *nc =='(' {
                                        self.tokens.push_back(SplitterState::Source(buffer.clone()));
                                        buffer.clear();
                                        SplitterState::Start
                                    }else if c== '\n'{
                                         SplitterState::Source(String::new())
                                    }else{
                                        SplitterState::Comment
                                    }
                                },
                                None =>{
                                    if c == '\n'{
                                        SplitterState::Source(String::new())
                                    }else{
                                        SplitterState::Comment
                                    }
                                },
                            }
                        },
                        SplitterState::Tag(_)=>{
                            if c == ']'{
                                self.tokens.push_back( SplitterState::Tag(buffer.clone()) );
                                buffer.clear();
                                SplitterState::Comment
                            }else if c == '\n'{
                                buffer.clear();
                                SplitterState::Source(String::new())
                            }else{
                                if c!= '['{
                                    buffer.push(c);
                                }
                                SplitterState::Tag(String::new())
                            }
                        },
                        SplitterState::Source(_) =>{
                            match iterator.peek(){
                                Some( (_j,nc) ) => {
                                    if c == '/' && *nc == '/'{
                                        SplitterState::Comment
                                    }else{
                                        buffer.push(c);
                                        SplitterState::Source(String::new())
                                    }
                                },
                                None => {
                                    buffer.push(c); 
                                    SplitterState::Source(String::new())
                                }
                            }
                        }
                    };
                    true
                },
                None => {
                    false
                }
            };
        }
        
        self
    }
}

pub fn load_text_file(path:String)->Result<String,&'static str>{
    match File::open(String::from(path)){
        Ok(file)=>{
            let mut reader = BufReader::new(file);
            let mut shader_source = String::new(); 
            match reader.read_to_string(&mut shader_source){
                Ok(_len)=> Ok(shader_source),
                Err(_fail)=> Err("An error occurred in read_to_string(...)")
            }
        },
        Err(_fail)=> Err("An error occurred reading the file")
    }
}