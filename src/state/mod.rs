#![allow(dead_code)]
extern crate gl;
extern crate queue_lib;
extern crate nalgebra as na; 

pub mod view;

use std::collections::VecDeque;
use std::mem::*;
use std::fs::File;
use na::{Matrix4,Vector2,Vector3,Perspective3};
use view::Camera;

use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader};

use super::graphics_util::program::*; 
use super::graphics_util::model::Model;
use super::string_util;
use super::graphics_util;

static mut WGL_CONTEXT:Option<*const WebGlRenderingContext> = None;

use wasm_bindgen::prelude::*;

pub enum STUB{ STUB }

pub struct ProgramState{
    window_size:Vector2<i32>,
    camera:Camera,
    events:EventState,
    glutin_comps:GlutinComponents,
    running:bool,
    wgl_context:Option<WebGlRenderingContext>,
}

#[wasm_bindgen]
extern {
    fn alert(s: &str);
}


impl ProgramState{
    pub fn new()->ProgramState{
        ProgramState{
            running:true,
            camera:Camera::new(),
            window_size:Vector2::new(0,0),
            events:EventState::new(),
            glutin_comps:GlutinComponents::new(),
            wgl_context:None, 
        }
    }

    
    pub fn init(&mut self){
        self.init_glutin();
        self.init_webgl();
    }

   
    pub fn init_webgl(&mut self){
        #[cfg(not(any(target_arch="x86_64",target_arch="x86",target_arch="arm")))]{  
        
            let document = web_sys::window().unwrap().document().unwrap();
            let canvas = document.get_element_by_id("restorids-canvas").unwrap();
            let canvas: web_sys::HtmlCanvasElement = canvas.dyn_into::<web_sys::HtmlCanvasElement>().unwrap();

            let context = canvas
                .get_context("webgl").unwrap()
                .unwrap()
                .dyn_into::<WebGlRenderingContext>().unwrap();

            self.wgl_context = Some(context);
            unsafe {
                WGL_CONTEXT =  Some(  self.wgl_context.as_ref().unwrap()  as * const WebGlRenderingContext) ;
            }
        }
    }

    fn init_glutin(&mut self){
        #[cfg(any(target_arch="x86_64",target_arch="x86",target_arch="arm"))]{
            use glutin::dpi::*;
            let events_loop = glutin::EventsLoop::new(); 

            let window = glutin::WindowBuilder::new()
                            .with_title("rust graphics")
                            .with_dimensions(LogicalSize::new(1024.0,768.0));

            let windowed_context = glutin::ContextBuilder::new()
                .with_vsync(true)
                .with_gl_profile(glutin::GlProfile::Compatibility)
                .build_windowed(window,&events_loop)
                .unwrap();

            let gl_window= unsafe{ windowed_context.make_current().unwrap()};
            gl::load_with(|symbol|{
                // println!("{}",symbol); 
                gl_window.get_proc_address(symbol) as *const _
            });

            self.glutin_comps.gl_window = Some(gl_window);
            self.events.glutin_events_loop = Some(events_loop);

        }
    }

    pub fn run(&mut self){
        unsafe{
            gl::ClearColor(0.0,0.0,0.0,1.0);
            gl::Viewport(0,0,1024,768);
            gl::Enable(gl::DEPTH_TEST);
        }

        let decoder = png::Decoder::new(File::open("./assets/images/rainbow.png").unwrap());
        let (info,mut reader ) = decoder.read_info().unwrap(); 
        
        println!("samples = {}, bit depth = {}, line_size = {}",
        info.color_type.samples(),
        info.bit_depth as i32,
        info.line_size);
        
        let mut buf = vec![0;info.buffer_size()];
        reader.next_frame(&mut buf).unwrap();

        let mut tex_id = 0; 
        unsafe{
            gl::GenTextures(1,&mut tex_id);
            gl::BindTexture(gl::TEXTURE_2D,tex_id);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::REPEAT as i32);	
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::REPEAT as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
            gl::TexImage2D(gl::TEXTURE_2D,0,gl::RGB as i32,info.width as i32,info.height as i32,0,gl::RGB,gl::UNSIGNED_BYTE,transmute(buf.as_ptr()));
            gl::GenerateMipmap(gl::TEXTURE_2D);
        }

        let shader_source= string_util::load_text_file("./assets/shaders/test.glsl".to_string()).unwrap();    
        let prog = Program::new().with_source(shader_source).compile_program().unwrap();

        let sphere = graphics_util::model::new_sphere(50.0,64,64, Vector3::new(0.,0.,0.));
        let persp = Perspective3::new(1.0f32,3.14159f32/5.0f32,1.0f32,10000.0f32).as_matrix().clone();

   
        let mut dt:f32 = 0.0; 
        let mut translation = Matrix4::zeros(); 
    
        while self.running{
            self.handle_events();
            
            unsafe{
                gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);
                prog.attach();        

                translation = na::geometry::Translation3::new(0.,0.,-350.).to_homogeneous();       
                let mut modelview = translation*Matrix4::from_axis_angle(&Vector3::y_axis(),3.14159*2.0*(dt*0.1).sin());
                modelview *= Matrix4::from_axis_angle(&Vector3::x_axis(),2.0*3.14159*(dt*0.09).sin());
                gl::UniformMatrix4fv(prog.get_uniform_index(Uniform::ModelView).unwrap() ,1,gl::FALSE, modelview.as_ptr() );
                gl::UniformMatrix4fv(prog.get_uniform_index(Uniform::MvNormal).unwrap(),1,gl::FALSE,  modelview.try_inverse().unwrap().transpose().as_ptr() );
                gl::UniformMatrix4fv(prog.get_uniform_index(Uniform::Projection).unwrap(),1,gl::FALSE, persp.as_ptr() ); 
                gl::UniformMatrix4fv(prog.get_uniform_index(Uniform::View).unwrap(),1,gl::FALSE, transmute(Matrix4::<f32>::identity().as_ptr()) );
                sphere.render(gl::TRIANGLES);            
                prog.detach();
                dt+=0.1;
            }
            self.swap_buffers();
        }
    }

    pub fn swap_buffers(&mut self){
        //if not wasm we just use glutins utilities 
        #[cfg(any(target_arch="x86_64",target_arch="x86",target_arch="arm"))]{
            self.glutin_comps.gl_window.as_ref().unwrap().swap_buffers().unwrap();
        }

        //otherwise it is assumed that we ARE in wasm a target then we gotta talk to 
        //javascript to swap the buffers 
        #[cfg(not(any(target_arch="x86_64",target_arch="x86",target_arch="arm")))]{
            //do nothing for now 
        }
    }
    ///A generic event handler 
    /// 
    pub fn handle_events(&mut self) {
        self.events.collect_events();
        let queue = &self.events.queue;
        let mut iter = queue.iter();        
        loop{
            let opt = iter.next();
            match opt{
                Some(event)=>{
                    match event{
                         GenericEvent::CloseRequested => self.running = false,
                        _ =>(),
                    }
                },
                None=>break
            }
        }
        self.events.queue.clear();
    }
}

pub struct GlutinComponents{   
    #[cfg(any(target_arch="x86_64",target_arch="x86",target_arch="arm") )]
    pub gl_window:Option<glutin::ContextWrapper<glutin::PossiblyCurrent,glutin::Window>>,
    
    #[cfg(not(any(target_arch="x86_64",target_arch="x86",target_arch="arm")))]
    pub gl_window:Option<STUB>,
}

impl GlutinComponents{
    pub fn new()->GlutinComponents{
        GlutinComponents{
            gl_window:None,
        }
    }
}

pub enum GenericEvent{
    CloseRequested,
    MouseMotion,
    MouseUp,
    MouseDown,
    TouchMotion, 
    TouchUp,
    TouchDown,
    NothingHappend, 
}

pub struct EventState{
    queue:VecDeque<GenericEvent>,
    #[cfg(any(target_arch="x86_64",target_arch="x86",target_arch="arm"))]
    glutin_events_loop:Option<glutin::EventsLoop>,

    //if wasm insert a STUB
    #[cfg(not(any(target_arch="x86_64",target_arch="x86",target_arch="arm")))]
    glutin_events_loop:Option<STUB>,
}

impl  EventState{
    pub fn new()->EventState{
        EventState{
            queue:VecDeque::new(),
            glutin_events_loop:None,
        }
    }

    pub fn collect_events(&mut self){
        if cfg!(any(target_arch ="x86",target_arch="x86_64")){
            self.glutin_collect_events();
        }else{
            self.wasm_collect_events();
        }
    }

    fn glutin_collect_events(&mut self){
        #[cfg( any(target_arch="x86_64",target_arch="x86",target_arch="arm") )]{
            let mut output_event = GenericEvent::NothingHappend; 
            let events_loop = self.glutin_events_loop.as_mut().unwrap();
            events_loop.poll_events( |event| {
                output_event = match event{
                    glutin::Event::WindowEvent{ event, .. } => match event {
                        glutin::WindowEvent::CloseRequested => GenericEvent::CloseRequested, 
                        _=> GenericEvent::NothingHappend
                    }, 
                    _ => GenericEvent::NothingHappend
                };
            });
            self.queue.push_back(output_event);
        }
    }

    fn wasm_collect_events(&mut self){
        let mut output_event = GenericEvent::NothingHappend;
        self.queue.push_back(output_event);
    }
}
pub mod webgl{
    use wasm_bindgen::prelude::*;
    use wasm_bindgen::JsCast;
    use web_sys::{WebGlProgram, WebGlRenderingContext, WebGlShader};
    use gl::types::*;
    
    pub unsafe fn BindBuffer(target:GLenum,buffer:GLuint){
        let context_ptr = super::WGL_CONTEXT.clone().unwrap().as_ref().unwrap();
        // context_ptr.bind_buffer(target,buffer);
    }
}
