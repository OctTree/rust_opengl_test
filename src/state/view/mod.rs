#![allow(dead_code)]

use nalgebra::{Matrix4,Vector3,Vector4};

pub struct ViewState{
    persp:Matrix4<f32> 
}

pub struct Camera{
    p:Vector3<f32>,
    q:Vector3<f32>,
    r:Vector3<f32>,
    o:Vector3<f32>,
    pub projection:Matrix4<f32>, 
    pub modelview:Matrix4<f32>,
}

impl Camera{
    pub fn new()->Camera{
        Camera{
            p:Vector3::new(1.,0.,0.),
            q:Vector3::new(0.,1.,0.),
            r:Vector3::new(0.,0.,1.),
            o:Vector3::new(0.,0.,0.),
            projection:Matrix4::identity(),
            modelview:Matrix4::identity(),
        }
    }
    
    pub fn look_at(&mut self, _dir:Vector3<f32>,_up:Vector3<f32>){
        
    }

    pub fn to_eye_space(&self)->Matrix4<f32>{
        let p = &self.p;
        let q = &self.q;
        let r = &self.r;
        let o = &self.o;
        let elems = [
            Vector4::<f32>::new(p.x,p.y,p.z,-p.dot(o)),
            Vector4::<f32>::new(q.x,q.y,q.z,-q.dot(o)),
            Vector4::<f32>::new(r.x,r.y,r.z,-r.dot(o)),
            Vector4::<f32>::new(0.0,0.0,0.0,  1.0    ),
        ];
        Matrix4::from_columns(&elems)
    }
}