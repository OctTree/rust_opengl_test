pub mod string_util;
pub mod graphics_util;
pub mod state; 

fn main() {
    let mut state = state::ProgramState::new();
    state.init();
    state.run();
}