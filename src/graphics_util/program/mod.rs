#![allow(dead_code)]

use super::super::string_util::*;
use std::mem::*;

static PROJECTION:usize = 0x0;
static MODEL_VIEW:usize = 0x1; 
static VIEW:usize  =0x2;  
static MAIN_TEXTURE:usize = 0x3; 
static MV_NORMAL:usize = 0x04; 

static ATTTIB_LIST:[&'static str;6] = [
    "vert_in\0\0",
    "normal_in\0\0",
    "uv_in\0\0",
    "color_in\0\0",
    "tangent_in\0\0",
    "bitangent_in\0\0"
];


pub enum Uniform{
    Projection,
    ModelView,
    View,
    MainTexture,
    MvNormal,
}

pub struct Program{
    pub id:u32, 
    source:Option<String>,
    uniforms:[i32;16],
}

impl Program{
    pub fn new()->Program{
        Program{
            id:0,
            source:None, 
            uniforms:[-1;16],
        }
    }
    pub fn with_source(self,source:String)->Program{
        Program{
            id:self.id,
            source:Some(source),
            uniforms:self.uniforms,
        }
    }
    pub fn compile_program(mut self) -> Result<Program,String>{
        if self.source.is_none(){
            return Err("No source was attached!".to_string());
        }

        unsafe{
            self.id = gl::CreateProgram();
            gl::UseProgram(self.id);
        }

        let mut compile_error = "Shader failed to compile:\n".to_string();
        let mut splitter = Splitter::new().parse_source(self.source.as_ref().unwrap());
        let mut shaders:Vec<u32> = Vec::new(); 

        //Loop through shader sources  the create and compile each one 
        while splitter.tokens.is_empty() == false{
            let tag   = match splitter.tokens.pop_front().unwrap() { SplitterState::Tag(tstr   )=> tstr , _ => String::new() };
            let source= match splitter.tokens.pop_front().unwrap() { SplitterState::Source(sstr)=> sstr , _ => String::new() };
            println!("\ntag=>{}\nsource:\n{}",tag,source);
            
            unsafe{ 
                let len = source.len() as i32; 
                let shader_id = if tag == "FRAG" {
                    gl::CreateShader(gl::FRAGMENT_SHADER)
                }else if tag == "VERT"{
                    gl::CreateShader(gl::VERTEX_SHADER)  
                }else{
                    return Err("Encountered invalid tag".to_string());
                };

                gl::ShaderSource(shader_id, 1 , &(source.as_ptr() as *const i8) , &len as *const i32);
                gl::CompileShader(shader_id);
                let status = get_shader_param(shader_id, gl::COMPILE_STATUS);

                if status as u8 == gl::FALSE{
                    let error_len = get_shader_param(shader_id,gl::INFO_LOG_LENGTH);
                    let error_log:String = (0..error_len).map(|_|'\0').collect();
                    gl::GetShaderInfoLog(shader_id,error_len,transmute(0 as usize),error_log.as_ptr() as *mut i8);
                    compile_error+=&error_log[..];
                    shaders.iter().for_each(|s| gl::DeleteShader(*s));
                    return Err(compile_error);
                }else{
                    shaders.push(shader_id);
                    gl::AttachShader(self.id,shader_id);
                }
            }
        }

        unsafe{
            self.bind_locations(self.id);
            gl::LinkProgram(self.id);
            let mut link_status = 0;
            gl::GetProgramiv(self.id, gl::LINK_STATUS,&mut link_status);
        
            if link_status as u8 == gl::FALSE{
                return Err(String::from("link failed")); 
            }else{
                self.get_uniform_locations();
                self.output_uniform_warnings();
            }

            shaders.iter().for_each(|s| gl::DeleteShader(*s));
        }



        Ok(self)
    }

    pub fn get_uniform_index(&self,u:Uniform)->Result<i32,&'static str>{
        match u {
            Uniform::Projection=>{
                let u = self.uniforms[PROJECTION];
                if u >= 0{ Ok(u) }else{ Err("Projection uniform not found!") }
            },
            Uniform::ModelView =>{
                let u = self.uniforms[MODEL_VIEW];
                if u >= 0 { Ok(u) }else{ Err("ModelView uniform not found!") }
            },
            Uniform::View =>{
                let u = self.uniforms[VIEW];
                if u >= 0 { Ok(u) }else{ Err("view uniform not found!") }
            },
            Uniform::MainTexture=>{
                let u = self.uniforms[MAIN_TEXTURE];
                if u>=0 { Ok(u) }else{ Err("main_texture uniform sampler not found in shader") }
            },
            Uniform::MvNormal=>{
                 let u = self.uniforms[MV_NORMAL];
                if u>=0 { Ok(u) }else{ Err("mv_normal uniform not found in shader") }
            }
        }
    }

    pub fn attach(&self){
        unsafe{
            gl::UseProgram(self.id);
        }
    }
    
    pub fn detach(&self){
        unsafe{
            gl::UseProgram(0);
        }
    }

    fn bind_locations(&self,id:u32){
        for (i,attr_str) in ATTTIB_LIST.iter().enumerate(){
            unsafe{
                gl::BindAttribLocation(id,i as u32,attr_str.as_ptr() as *const i8);
            }
        }        
    }

    fn get_uniform_locations(&mut self){
        unsafe{
            gl::UseProgram(self.id);
            self.uniforms[VIEW]= gl::GetUniformLocation(self.id,"view\0".as_ptr() as *const i8);
            self.uniforms[PROJECTION]= gl::GetUniformLocation(self.id,"projection\0\0".as_ptr() as *const i8);
            self.uniforms[MODEL_VIEW] = gl::GetUniformLocation(self.id,"modelview\0\0".as_ptr() as *const i8);
            self.uniforms[MAIN_TEXTURE]= gl::GetUniformLocation(self.id,"main_texture\0\0".as_ptr() as *const i8);
            self.uniforms[MV_NORMAL]= gl::GetUniformLocation(self.id,"mv_normal\0\0".as_ptr() as *const i8);
        }
    }

    fn output_uniform_warnings(&self){
        if self.uniforms[VIEW] == -1{
            println!("WARNING:'view' uniform not found!");
        }
        if self.uniforms[PROJECTION] == -1{
            println!("WARNING:'projection' uniform not found!");
        }
        if self.uniforms[MODEL_VIEW] == -1{
            println!("WARNING:'modelview' uniform not found!");
        }
        if self.uniforms[MAIN_TEXTURE] == -1{
            println!("WARNING:'main_texture' uniform not found!");
        }
        if self.uniforms[MV_NORMAL] == -1{
            println!("WARNING:'mv_normal' uniform not found!");
        }
    }
}

fn get_shader_param(shader_id:u32,param_type:u32)->i32{
    let param:i32 = 0; 
    unsafe{
        gl::GetShaderiv(shader_id,param_type,transmute(&param))
    }
    param
}


