#![allow(dead_code)]

use std::mem::*; 

pub struct BufferObject<T>{
    id:u32,
    target:u32,
    usage:u32,
    pub buffer:Vec<T>, 
}

pub struct BoundBufferObject<'a,T>{
    buf_obj:&'a BufferObject<T>
}

impl<T:Copy> BufferObject<T>{
    pub fn new(target:u32,usage:u32)->BufferObject<T>{
        BufferObject{
            id:0, 
            target, 
            usage,
            buffer:Vec::new(),
        }
    }

    pub fn with_buffer(self,buffer:Vec<T>)->BufferObject<T>{
         BufferObject{
            id:self.id, 
            target:self.target, 
            usage:self.usage,
            buffer,
        }
    }

    pub fn gen(self)->BufferObject<T>{
        let mut id = 0; 
        

        unsafe{ 
            gl::GenBuffers(1,&mut id); 
            gl::BindBuffer(self.target,id);
        }
        BufferObject{
            id:id, 
            target:self.target, 
            usage:self.usage,
            buffer:self.buffer,
        }
    }
 

    pub fn bind(&self){
        self.inner_bind();
    }

    pub fn unbind(&self){
        unsafe{
            gl::BindBuffer(self.target,0);
        }
    }

    pub fn allocate(self)->BufferObject<T>{
        let size = size_of::<T>()*self.buffer.len();
        unsafe{
            gl::BufferData(self.target,transmute(size),transmute(self.buffer.as_ptr()),self.usage);
        }
        self
    }

    pub fn update(&mut self){
        self.inner_bind(); 
        unsafe{
            gl::BufferSubData(
                self.target,
                transmute(0 as usize),
                transmute(self.buffer.len()*size_of::<T>()),
                transmute(self.buffer.as_ptr())
            );
        }
    }
    fn inner_bind(&self){
        unsafe{
            gl::BindBuffer(self.target,self.id);
        }
    }
}

impl <'a,T> BoundBufferObject<'a,T>{
    pub fn new(buf_obj:&'a BufferObject<T>)->BoundBufferObject<T>{
        BoundBufferObject{
            buf_obj
        }
    }
}

impl <'a,T> Drop for BoundBufferObject<'a,T>{
    fn drop(&mut self){
        unsafe{
            gl::BindBuffer(self.buf_obj.target,0);
        }
    }
}
