use super::buffer_object::*;
use std::mem::transmute;
use nalgebra::{Vector2,Vector3,Vector4};

pub struct Attribute<T>{
    buf_obj:Option<BufferObject<T>>,
    attrib_index:u32, 
    components:i32,
}

pub trait ModelComponent{
    fn bind(&self);
    fn unbind(&self);
    fn len(&self)->usize; 
}

impl <T> Attribute<T>{
        pub fn new()->Attribute<T>{
        Attribute{
            buf_obj:None,
            attrib_index:0,
            components:1,
        }
    }

    pub fn with_buffer(self,bo:BufferObject<T>,components:i32)->Attribute<T>{
        Attribute{
            buf_obj:Some(bo),
            attrib_index:self.attrib_index,
            components,
        }
    }

    pub fn with_index(self,index:u32)->Attribute<T>{
        Attribute{
            buf_obj:self.buf_obj,
            attrib_index:index,
            components:self.components,
        }
    }
}

impl ModelComponent for Attribute<f32>{
    fn bind(&self){
        self.buf_obj.as_ref().unwrap().bind();
        unsafe{
            gl::EnableVertexAttribArray(self.attrib_index);
            gl::VertexAttribPointer(
                self.attrib_index,
                self.components,
                gl::FLOAT,
                gl::FALSE,
                std::mem::transmute(0),
                transmute(0 as usize)
            );
        }
    }

    fn unbind(&self){
        unsafe{
            gl::DisableVertexAttribArray(self.attrib_index);
        }
         self.buf_obj.as_ref().unwrap().unbind();
    }

    fn len(&self)->usize{
        let buffer = &self.buf_obj.as_ref().unwrap().buffer;
        buffer.len()/(self.components as usize)
    }
}

impl ModelComponent for Attribute<i32>{
    fn bind(&self){
        self.buf_obj.as_ref().unwrap().bind();
    }

    fn unbind(&self){
        self.buf_obj.as_ref().unwrap().unbind();
    }

    fn len(&self)->usize{
        let buffer = &self.buf_obj.as_ref().unwrap().buffer;
        buffer.len()/(self.components as usize)
    }
}


impl ModelComponent for Attribute<Vector3<f32>>{
    fn bind(&self){
        self.buf_obj.as_ref().unwrap().bind();
        unsafe{
            gl::EnableVertexAttribArray(self.attrib_index);
            gl::VertexAttribPointer(
                self.attrib_index,
                self.components,
                gl::FLOAT,
                gl::FALSE,
                std::mem::transmute(0),
                transmute(0 as usize)
            );
        }
    }

    fn unbind(&self){
        unsafe{
            gl::DisableVertexAttribArray(self.attrib_index);
        }
         self.buf_obj.as_ref().unwrap().unbind();
    }

    fn len(&self)->usize{
        let buffer = &self.buf_obj.as_ref().unwrap().buffer;
        buffer.len()
    }
}

impl ModelComponent for Attribute<Vector2<f32>>{
    fn bind(&self){
        self.buf_obj.as_ref().unwrap().bind();
        unsafe{
            gl::EnableVertexAttribArray(self.attrib_index);
            gl::VertexAttribPointer(
                self.attrib_index,
                self.components,
                gl::FLOAT,
                gl::FALSE,
                std::mem::transmute(0),
                transmute(0 as usize)
            );
        }
    }

    fn unbind(&self){
        unsafe{
            gl::DisableVertexAttribArray(self.attrib_index);
        }
         self.buf_obj.as_ref().unwrap().unbind();
    }

    fn len(&self)->usize{
        let buffer = &self.buf_obj.as_ref().unwrap().buffer;
        buffer.len()
    }
}

impl ModelComponent for Attribute<Vector4<f32>>{
    fn bind(&self){
        self.buf_obj.as_ref().unwrap().bind();
        unsafe{
            gl::EnableVertexAttribArray(self.attrib_index);
            gl::VertexAttribPointer(
                self.attrib_index,
                self.components,
                gl::FLOAT,
                gl::FALSE,
                std::mem::transmute(0),
                transmute(0 as usize)
            );
        }
    }

    fn unbind(&self){
        unsafe{
            gl::DisableVertexAttribArray(self.attrib_index);
        }
         self.buf_obj.as_ref().unwrap().unbind();
    }

    fn len(&self)->usize{
        let buffer = &self.buf_obj.as_ref().unwrap().buffer;
        buffer.len()
    }
}