#![allow(dead_code)]

use super::attribute::*;
use super::buffer_object::{BufferObject};

use gl::types::{GLenum};
use std::mem::*;
use std::cmp::*;
use nalgebra::{Vector2,Vector3,Vector4,Matrix4};
use std::f32::consts::{PI};

pub trait Model{
    fn render(&self,mode:GLenum); 
}

struct RiggedModel{
    
}

pub struct StaticModel{
    component_list:Vec<Box<dyn ModelComponent>>,
    num_elements:i32,
}
impl StaticModel{
    pub fn new()->StaticModel{
        StaticModel{
            component_list:Vec::new(),
            num_elements:1,
        }
    }
    pub fn add_component(mut self,comp:Box<dyn ModelComponent>)->StaticModel{
        self.component_list.push(comp);
        self
    }
    pub fn with_element_count(mut self,count:i32)->StaticModel{
        self.num_elements = count; 
        self
    }
}

impl Model for StaticModel{
    fn render(&self,mode:GLenum){
        self.component_list.iter().for_each(|comp| comp.bind());
        unsafe{
            gl::DrawElements(mode,self.num_elements,gl::UNSIGNED_INT,transmute(0 as usize));
        }
        self.component_list.iter().for_each(|comp| comp.unbind());
    }
}


pub fn new_sphere(radius:f32,slices:i32,wedges:i32,center:Vector3::<f32>)->Box<StaticModel>{
    let mut vertex_list:Vec<Vector4::<f32>> = Vec::new(); 
    let mut uv_list:Vec<Vector2::<f32>> = Vec::new(); 
    let mut index_list:Vec<i32> = Vec::new(); 
    let mut slice:Vec<Vector4::<f32>> = Vec::new(); 
    let mut slice_v:Vec<f32> = Vec::new(); //v tex coord for slice 
    
    //create slice 
    let slice_delta = (PI)/(slices as f32);
   
    for i in 0..=slices{
        let angle = slice_delta*(i as f32)-(PI/2.0);
        //vtc => v tex coordinate
        let vtc = angle/PI + 0.5f32;
        let slice_point = Vector4::new(radius*angle.cos(),radius*angle.sin(),0.0,1.0);
        slice.push(slice_point);
        slice_v.push(vtc);
    }

    let wedge_delta = (2.0*PI)/(wedges as f32);
    let mut vertex_index = 0; 
    for i in 0..wedges{
        let angle = wedge_delta*(i as f32);
        let l =Matrix4::from_axis_angle(&Vector3::y_axis(),angle);
        let u =Matrix4::from_axis_angle(&Vector3::y_axis(),angle+wedge_delta);
        
        //luc => left u tex coord , ruc => right u tex coord
        let luc = angle/(PI*2.0);
        let ruc = (angle+wedge_delta)/(PI*2.0); 

        for j in 1..slice.len(){
            //compute sphere wedge
            let bl = l*slice[j-1]; 
            let br = u*slice[j-1];
            let tl = l*slice[j];
            let tr = u*slice[j]; 
            //bottom v coord
            let bvc = slice_v[j-1];
            //top v coord
            let tvc = slice_v[j];

            let bl_in = vertex_index + 0; 
            let br_in = vertex_index + 1; 
            let tr_in = vertex_index + 2; 
            let tl_in = vertex_index + 3;

            //first triangle 
            vertex_list.push(bl);
            uv_list.push(Vector2::new(luc,bvc));
                
            vertex_list.push(br);
            uv_list.push(Vector2::new(ruc,bvc));
            
            vertex_list.push(tr);
            uv_list.push(Vector2::new(ruc,tvc));
      
            vertex_list.push(tl);
            uv_list.push(Vector2::new(luc,tvc));
      
            //push triangle 1 
            index_list.push(bl_in);
            index_list.push(br_in);
            index_list.push(tr_in);

            //push triangle 2 
            index_list.push(bl_in);
            index_list.push(tr_in);
            index_list.push(tl_in);

            vertex_index+=4;
        }
    }

    let elem_count = index_list.len() ;

    //translate everything by the center
    for vert in vertex_list.iter_mut(){
        (*vert)+=Vector4::new(center.x,center.y,center.z,0.0); 
    }

    //create all buffer objects
    let normals = calculate_normals(&vertex_list,&index_list);


    let total_size = normals.len()*size_of::<Vector3<f32>>() + 
    vertex_list.len()*size_of::<Vector4<f32>>() +
    uv_list.len()*size_of::<Vector2<f32>>() + 
    index_list.len()*size_of::<i32>() ;
    println!("Memmory allocated for sphere : {} bytes",total_size);




    let vert_bo = BufferObject::new(gl::ARRAY_BUFFER, gl::STATIC_DRAW).with_buffer(vertex_list).gen().allocate();
    let normal_bo = BufferObject::new(gl::ARRAY_BUFFER,gl::STATIC_DRAW).with_buffer(normals).gen().allocate();
    let uv_bo = BufferObject::new(gl::ARRAY_BUFFER,gl::STATIC_DRAW).with_buffer(uv_list).gen().allocate();    
    let index_bo = BufferObject::new(gl::ELEMENT_ARRAY_BUFFER,gl::STATIC_DRAW).with_buffer(index_list).gen().allocate();



    
    
    //create a static model of a sphere 
    Box::new(
        StaticModel::new()
        .add_component(Box::new(
            Attribute::new().with_buffer(vert_bo,4).with_index(0)
        ))
        .add_component(Box::new(
            Attribute::new().with_buffer(normal_bo,3).with_index(1)
        ))
        .add_component(Box::new(
            Attribute::new().with_buffer(uv_bo,2).with_index(2)
        ))
        .add_component(Box::new(
            Attribute::new().with_buffer(index_bo,1)
        ))
        .with_element_count(elem_count as i32)
    )
}

///A proc that creates a new triangle, for debugging purposes
pub fn new_triangle()->Box<StaticModel>{
    let vertex_list:Vec<f32> = vec![ -20.0,0.0,0.,1.0,
                                      0.0,20.0,0.,1.0,
                                      20.0,0.0,0.,1.0,
                                    ];

    let color_list:Vec<f32> = vec![1.,0.,0.,0.,
                                   0.,1.,0.,0.,
                                   0.,0.,1.,0.];

    let index_list:Vec<i32> = vec![0,1,2];
    
    let vbo = BufferObject::new(gl::ARRAY_BUFFER,gl::STATIC_DRAW).with_buffer(vertex_list).gen().allocate();
    let cbo = BufferObject::new(gl::ARRAY_BUFFER,gl::STATIC_DRAW).with_buffer(color_list).gen().allocate();
    let ibo = BufferObject::new(gl::ELEMENT_ARRAY_BUFFER,gl::STATIC_DRAW).with_buffer(index_list).gen().allocate();
    
    Box::new( 
        StaticModel::new()
        .add_component( Box::new(
            Attribute::new().with_buffer(vbo,4).with_index(0)
        ))
        .add_component( Box::new(
            Attribute::new().with_buffer(cbo,4).with_index(3)
        ))
        .add_component( Box::new(
            Attribute::new().with_buffer(ibo,1)
        ))
        .with_element_count(3)
    )
}

pub fn calculate_normals(vertex_list:&Vec<Vector4::<f32>>,index_list:&Vec<i32>)->Vec<Vector3<f32>>{
    let mut normal_list:Vec<Vector3::<f32>>=vertex_list.iter().map(|_| nalgebra::zero() ).collect();
    
    let mut i = 0; 
    while i < index_list.len(){
        let il0 = index_list[i+0] as usize; 
        let il1 = index_list[i+1] as usize; 
        let il2 = index_list[i+2] as usize; 

        let t0  = to_3d(vertex_list[il0]);
        let t1  = to_3d(vertex_list[il1]);
        let t2  = to_3d(vertex_list[il2]);

        // let a = t1-t0; 
        // let b = t2-t0; 
        // let norm = a.cross(&b);
        // normal_list[il0]+=norm;
        // normal_list[il1]+=norm;
        // normal_list[il2]+=norm;
        
        
        normal_list[il0]= t0.normalize();
        normal_list[il1]= t1.normalize();
        normal_list[il2]= t2.normalize();

        i+=3; 
    }

    for i in 0..normal_list.len(){ 
        normal_list[i]=normal_list[i].normalize();
    }

    normal_list
}
fn to_3d(v4:Vector4<f32>)->Vector3<f32>{
    Vector3::new(v4.x,v4.y,v4.z)
}


            // //first triangle 
            // vertex_list.push(bl);
            // uv_list.push(Vector2::new(luc,bvc));
            // index_list.push(vertex_index);
            // vertex_index+=1;
            
            // vertex_list.push(br);
            // uv_list.push(Vector2::new(ruc,bvc));
            // index_list.push(vertex_index);
            // vertex_index+=1;
            
            // vertex_list.push(tr);
            // uv_list.push(Vector2::new(ruc,tvc));
            // index_list.push(vertex_index);
            // vertex_index+=1;

            // //second triangle 
            // vertex_list.push(bl);
            // uv_list.push(Vector2::new(luc,bvc));
            // index_list.push(vertex_index);
            // vertex_index+=1;

            // vertex_list.push(tr);
            // uv_list.push(Vector2::new(ruc,tvc));
            // index_list.push(vertex_index);
            // vertex_index+=1;
            
            // vertex_list.push(tl);
            // uv_list.push(Vector2::new(luc,tvc));
            // index_list.push(vertex_index);
            // vertex_index+=1;