#[allow(unused_assignments)]
pub fn radix_sort(list:&mut Vec<i32>,lbound:i32,ubound:i32){
    let mut temp_buffer = list.clone(); 
    let mut temp; 
    let mut cur_ref = list; 
    let mut pre_ref = &mut temp_buffer; 
    let mut base = 1; 
    
    for _ in 0..4{
        counting_sort(lbound,ubound,base,cur_ref,pre_ref);
        temp = cur_ref; 
        cur_ref = pre_ref; 
        pre_ref = temp; 
        base*=10;
        counting_sort(lbound,ubound,base,cur_ref,pre_ref);
        temp = cur_ref; 
        cur_ref = pre_ref; 
        pre_ref = temp; 
        base*=10;
    }
    cur_ref.iter().enumerate().for_each(|(_,x)| print!("[{}]",x));
    println!("");
}

pub fn counting_sort(lbound:i32,ubound:i32,base:i32,list:&mut Vec<i32>,swap_buffer:&mut Vec<i32>){
    let mut count_table:[i32;10] = [0;10];
    for k in 0..list.len(){
        let digit = (list[k].abs()/base)%10;
        count_table[digit as usize]+=1;
    }
    let mut count_sum = 0;
    for k in 0..count_table.len(){
        count_sum+=count_table[k];
        count_table[k] = count_sum; 
    }
    let mut zero_index = 0;
    for k in 0..list.len(){
        let num = list[k];
        let key = ((num.abs()/base)%10)-1;
        if key<0{
            swap_buffer[zero_index] = num;
            zero_index+=1;
        }else{
            swap_buffer[count_table[key as usize] as usize] = num; 
            count_table[key as usize]+=1;
        }       
    }
}