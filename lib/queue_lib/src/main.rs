use queue_lib::queue::Heap;
use rand;



fn main(){
    let mut h:Heap<i32,i32> = Heap::new();
    for k in (0..30).map( |_|  rand::random()  )
                    .map( |x:i32| x.abs()%200 ){
        h.level_order_add(k,1);
    }
    
    h.build_heap();
    println!("{}",h);

    while h.size() > 0{
        match h.extract_extrema(){
            Some((key,_val)) => {
                println!("max key = {}" , key);
            },
            None =>{

            }
        }
    }
}