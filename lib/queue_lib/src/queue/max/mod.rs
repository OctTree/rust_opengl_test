use super::Heap;

pub struct MaxQueue<K,V>{
    heap:Heap<K,V>,
}

impl <K:PartialOrd+Clone+Default,V> MaxQueue<K,V>{
    pub fn new() -> MaxQueue<K,V>{
        MaxQueue{
            heap:Heap::new().with_max_heap(),
        }
    }
    
    pub fn insert(&mut self, key:K,val:V){
        self.heap.insert(key,val);
    }

    pub fn remove_max(&mut self)->Option<(K,Option<V>)>{
        self.heap.extract_extrema()
    }
}