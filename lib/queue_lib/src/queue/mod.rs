pub mod min;
pub mod max;
pub mod circular;

use std::fmt; 

///comparison function that is used by heao
pub trait HeapCompare<K:Default+PartialOrd>{
    fn compare(&self, a:&K,b:&K,is_max_heap:i32) ->i32{
        let result;
        if a==b{
            result = 0
        }else{
            if a > b {
                result = 1
            }else {
                result = -1
            }
        }
        result*is_max_heap
    }
}

pub struct Heap<K,V> {
    data:Vec<(K,Option<V>)>,
    is_max_heap:i32,
}

impl <K:Default+PartialOrd,V> HeapCompare<K> for Heap<K,V>{}

impl <K:fmt::Display+Clone+PartialOrd+Default,V> fmt::Display for Heap<K,V>{
    fn fmt(&self,f:&mut fmt::Formatter<'_>)->fmt::Result{
        let mut buf = String::new(); 
        fmt_helper(self,1,&mut buf, f)
    }
}

fn fmt_helper<K:fmt::Display+Clone+PartialOrd+Default,V>(h:&Heap<K,V>,k:usize,space_buffer:&mut String,f:&mut fmt::Formatter<'_>)->fmt::Result{
    if k > h.size() { 
        return fmt::Result::Ok(());
    }

    space_buffer.push('\t');        
    fmt_helper(h,right(k), space_buffer,f).unwrap();
    space_buffer.pop();

    write!(f,"{}{}\n",space_buffer,h.data[k].0).unwrap(); 
    
    space_buffer.push('\t');     
    fmt_helper(h,left(k), space_buffer,f).unwrap();
    space_buffer.pop();

    fmt::Result::Ok(())
}

impl <K:Default+PartialOrd+Clone,V> Heap<K,V> {
    pub fn new() -> Heap<K, V> {
        Heap {
            data: vec![(K::default(), None)],
            is_max_heap: 1,
        }
    }

    pub fn with_min_heap(mut self)->Heap<K,V>{
        self.is_max_heap = -1; 
        self
    }

    pub fn with_max_heap(mut self)->Heap<K,V>{
        self.is_max_heap = 1; 
        self
    }

    pub fn level_order_add(&mut self, k: K, v: V) {
        self.data.push((k, Some(v)));
    }

    pub fn insert(&mut self, k:K,v:V){
        self.level_order_add(k.clone(),v);
        self.change_key(self.size(),k);
    }

    pub fn extract_extrema(&mut self)->Option<(K,Option<V>)>{
        let size = self.size();
        if size >= 1{
            self.data.swap(size,1);
            let pair = self.data.pop();
            self.heapify(1);
            pair
        }else{
            None
        }
    }

    pub fn size(&self)->usize{
        self.data.len()-1
    }

    pub fn build_heap(&mut self){
        let size = self.size();
        for k in (1..=size/2).rev() {
            self.heapify(k);
        }
    }

    pub fn get_key(&self,i:usize)->&K{
        extract_key_from_tuple(& self.data, i)
    }

    pub fn search(&self,search_key:K)->Option<(&K,&Option<V>)>{
        for k in 1..= self.size() {
            let (key,val) = &self.data[k];
            if  &search_key == key  {
                return Some( (key,val) );
            }
        }
        None
    } 

    fn heapify(&mut self,i:usize){
        let size  = self.size() ;
        let left  = left(i) ;
        let right = right(i);
        let mut max_index:usize = i;

        if right <= size && self.compare( 
                                extract_key_from_tuple(&self.data,right),
                                extract_key_from_tuple(&self.data,max_index),
                                self.is_max_heap
                                ) > 0 {
            max_index = right;
        }

        if left <= size && self.compare(
                                extract_key_from_tuple(&self.data,left),
                                extract_key_from_tuple(&self.data,max_index),
                                self.is_max_heap 
                            ) > 0 {
            max_index = left;
        }

        if max_index != i {
            self.data.swap(max_index,i);
            self.heapify(max_index);
        }
    }

    fn change_key(&mut self,mut i:usize,key:K){
        self.data[i].0 = key; 
        while i > 1 && self.compare(
                            extract_key_from_tuple(&self.data, parent(i)),
                            extract_key_from_tuple(&self.data,i),
                            self.is_max_heap, 
                        ) < 0 {
            self.data.swap(i,parent(i));
            i = parent(i);
        } 
    }
}

fn extract_key_from_tuple<K,V>( buffer:&Vec<(K,Option<V>)>, i:usize ) -> &K{
    &((&buffer[i]).0)
}

fn left(i:usize)->usize{
    i*2
}

fn right(i:usize)->usize{
    i*2+1
}

fn parent(i:usize)->usize{
    i/2
}