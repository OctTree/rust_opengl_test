pub struct CircularQueue<V>{
    buffer:Option<Vec<V>>,
    lbound:usize,
    ubound:usize,
    capacity:usize,
    size:usize,
}

impl <V:Copy> CircularQueue<V>{
    pub fn new() -> CircularQueue<V>{
        CircularQueue{
            buffer: None,
            lbound:0,
            ubound:0,
            capacity:0,
            size:0,
        }
    }

    pub fn with_capacity(self,n:usize)->CircularQueue<V>{
        CircularQueue{
            buffer: Some( Vec::with_capacity(n)  ),
            lbound:self.lbound, 
            ubound:self.ubound,
            capacity:n,
            size:self.size,
        }
    }

    pub fn enqueue(&mut self,v:V){
        if self.size >= self.capacity {
            return
        }

        match  &mut self.buffer {
            Some(buffer) =>{ 
                if buffer.len() < self.capacity{
                    buffer.push(v) 
                }else{
                     buffer[self.ubound] = v
                }
            },
            None => () ,
        }
        self.ubound = (self.ubound+1) % self.capacity; 
        self.size+=1;
    }

    pub fn dequeue(&mut self) -> Option<V>{
        if self.size == 0 {
            return None
        }
        let result =  match  &mut self.buffer {
            Some(buffer) => Some(buffer[self.lbound]),
            None => None,
        };
        self.lbound = (self.lbound+1) % self.capacity;
        self.size-=1; 
        result
    }
    pub fn len(&self)->usize{
        self.size
    }
}