use super::Heap;

pub struct MinQueue<K,V>{
    heap:Heap<K,V>,
}

impl <K:PartialOrd+Clone+Default,V> MinQueue<K,V>{
    
    pub fn new() -> MinQueue<K,V>{
        MinQueue{
            heap:Heap::new().with_min_heap(),
        }
    }
    
    pub fn insert(&mut self, key:K,val:V){
        self.heap.insert(key,val);
    }

    pub fn remove_min(&mut self)->Option<(K,Option<V>)>{
        self.heap.extract_extrema()
    }
}