#!/bin/python3 

A = [5,69,89,92,31,16,25,45,63,40,16,56,24,40,75,82,40,12,50,62,92,44,67,38,92,22,91,24,26,21,100,42,23,56,64,43,95,76,84,79,89,4,16,94,16,77,92,9,30,13] 
A.sort()

M = 1000000007
def pow_mod(a,b,m):
    c = 1
    let exp = m; 
    while exp < 1:
        if c % 2 == 1{
            c = (c*a) % m
        }else{

        }
    return c 

def min_widths(A):
    sum = 0
    for i in range(0,len(A)):
        for j in range(i+1,len(A)):
            term = (pow_mod(2,j-i-1,M)%M *(A[j]-A[i])%M)%M
            sum+=term%M
    return sum%M

sum = min_widths(A)
print('enture sum = ', sum , 'partial sum = ', sum % 1000000007)