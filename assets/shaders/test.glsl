
//[VERT]
#version 150
attribute vec4 vert_in; 
attribute vec4 color_in; 
attribute vec3 normal_in; 
attribute vec2 uv_in; 

uniform mat4 view;
uniform mat4 modelview; 
uniform mat4 mv_normal; 
uniform mat4 projection;

varying vec4 vert_color_out; 
varying vec2 uv_out;
varying vec3 normal_out; 
varying vec3 eye_vert_out; 

void main(){
    vert_color_out = vec4(abs(normal_in.xyz),0.); 
    normal_out = (mv_normal*vec4(normal_in.xyz,0.)).xyz;  
    vec4 vert = modelview*vert_in;
    eye_vert_out = vert.xyz; 
    uv_out = uv_in; 
    gl_Position = projection*view*vert;
}
//(VERT)


//[FRAG]
#version 150
uniform sampler2D main_texture; 

varying vec4 vert_color_out; 
varying vec2 uv_out;
varying vec3 normal_out; 
varying vec3 eye_vert_out; 

vec3 light_pos = vec3(50,50,80);

void main(){
    vec3 n = normalize(normal_out);
    vec3 l = normalize(light_pos);
    vec3 v = -normalize(eye_vert_out);
    vec3 h = normalize(v+l); 
    
    float specular_coef = 512.; 
    float diffuse_comp = max(dot(n,l),0.0);
    float specular_comp = pow(max(dot(h,n),0.0),specular_coef);

    vec4 ambient = texture2D(main_texture,uv_out)*0.37;
    vec4 diffuse = ambient*diffuse_comp*1.5; 
    vec4 specular = ambient*specular_comp*0.001;
    
    gl_FragColor = mix(ambient+diffuse+specular_comp,vec4(1.)*(diffuse_comp+specular_comp),0.01); 
}
//(FRAG)